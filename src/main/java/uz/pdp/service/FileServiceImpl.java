package uz.pdp.service;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import uz.pdp.exeptions.ResourceTopilmadi;
import uz.pdp.model.*;
import uz.pdp.model.enums.FileName;
import uz.pdp.utills.AppConstants;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileServiceImpl implements FileService {
    @Override
    public String readFile(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        FileInputStream fi = new FileInputStream(file);
        Scanner scanner = new Scanner(fi);
        StringBuilder sb = new StringBuilder("");
        while (scanner.hasNext()) {
            sb.append(scanner.nextLine());
        }
        return sb.toString();
    }

    @Override
    public void addToFile(FileName fileName, Object object) throws IOException {
        Gson gson=new Gson();
        switch (fileName){
            case VOTE:
                Vote vote=(Vote)object;
                String s = readFile(AppConstants.BASE_URL + "vote.txt");
                List<Vote>voteList=gson.fromJson(s,new TypeToken<List<Vote>>(){}.getType());
                voteList.add(vote);
                String txt=gson.toJson(voteList);
                writeToFile(AppConstants.BASE_URL + "vote.txt",txt);
                break;
            case PARTY:
                Party party=(Party)object;
                String s1 = readFile(AppConstants.BASE_URL + "party.txt");
                List<Party>partyList=gson.fromJson(s1,new TypeToken<List<Party>>(){}.getType());
                if (partyList!=null){
                    partyList.add(party);
                }
                else {
                    partyList=new ArrayList<>();
                    partyList.add(party);
                }
                String txt1=gson.toJson(partyList);
                writeToFile(AppConstants.BASE_URL + "party.txt",txt1);
                break;
            case REGION:
                Region region=(Region) object;
                String s2 = readFile(AppConstants.BASE_URL + "region.txt");
                List<Region>regionList=gson.fromJson(s2,new TypeToken<List<Region>>(){}.getType());
                regionList.add(region);
                String txt2=gson.toJson(regionList);
                writeToFile(AppConstants.BASE_URL + "region.txt",txt2);
                break;
            case TG_USER:
                TgUser tgUser=(TgUser) object;
                String s3 = readFile(AppConstants.BASE_URL + "tgUser.txt");
                List<TgUser> tgUserList=gson.fromJson(s3,new TypeToken<List<TgUser>>(){}.getType());
                tgUserList.add(tgUser);
                String txt3=gson.toJson(tgUserList);
                writeToFile(AppConstants.BASE_URL + "tgUser.txt",txt3);
                break;
            case DISTRICT:
                District district=(District) object;
                String s4 = readFile(AppConstants.BASE_URL + "district.txt");
                List<District>districtList=gson.fromJson(s4,new TypeToken<List<District>>(){}.getType());
                districtList.add(district);
                String txt4=gson.toJson(districtList);
                writeToFile(AppConstants.BASE_URL + "district.txt",txt4);
                break;
            case CANDIDATE:
                Candidate candidate=(Candidate) object;
                String s5 = readFile(AppConstants.BASE_URL + "candidate.txt");
                List<Candidate>candidateList=gson.fromJson(s5,new TypeToken<List<Candidate>>(){}.getType());
                candidateList.add(candidate);
                String txt5=gson.toJson(candidateList);
                writeToFile(AppConstants.BASE_URL + "candidate.txt",txt5);
                break;
            case PARTY_PLAN:
                PartyPlan partyPlan=(PartyPlan) object;
                String s6 = readFile(AppConstants.BASE_URL + "partyPlan.txt");
                List<PartyPlan>partyPlansList=gson.fromJson(s6,new TypeToken<List<PartyPlan>>(){}.getType());
                partyPlansList.add(partyPlan);
                String txt6=gson.toJson(partyPlansList);
                writeToFile(AppConstants.BASE_URL + "partyPlan.txt",txt6);
                break;
            default:
                throw new ResourceTopilmadi("topilmadi!!!!!");

        }
    }

    @Override
    public void writeToFile(String filePath, String content) throws IOException {
        FileWriter writer=new FileWriter(filePath);
        writer.write(content);
        writer.close();
    }

    @Override
    public List<Object> getObjects(FileName fileName) throws FileNotFoundException {
        Gson gson=new Gson();
        switch (fileName){
            case VOTE:
                String s = readFile(AppConstants.BASE_URL + "vote.txt");
                return gson.fromJson(s,new TypeToken<List<Vote>>(){}.getType());
            case PARTY:
                String s1 = readFile(AppConstants.BASE_URL + "party.txt");
                return gson.fromJson(s1,new TypeToken<List<Party>>(){}.getType());
            case REGION:
                String s2 = readFile(AppConstants.BASE_URL + "region.txt");
                return gson.fromJson(s2,new TypeToken<List<Region>>(){}.getType());
            case TG_USER:
                String s3 = readFile(AppConstants.BASE_URL + "tgUser.txt");
                return gson.fromJson(s3,new TypeToken<List<TgUser>>(){}.getType());
            case DISTRICT:
                String s4 = readFile(AppConstants.BASE_URL + "district.txt");
                return gson.fromJson(s4,new TypeToken<List<District>>(){}.getType());
            case CANDIDATE:
                String s5 = readFile(AppConstants.BASE_URL + "candidate.txt");
                return gson.fromJson(s5,new TypeToken<List<Candidate>>(){}.getType());
            case PARTY_PLAN:
                String s6 = readFile(AppConstants.BASE_URL + "partyPlan.txt");
                return gson.fromJson(s6,new TypeToken<List<PartyPlan>>(){}.getType());
            default:
                throw new ResourceTopilmadi("topilmadi!!!!!");

        }
    }
}
