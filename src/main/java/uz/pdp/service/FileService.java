package uz.pdp.service;

import uz.pdp.exeptions.ResourceTopilmadi;
import uz.pdp.model.enums.FileName;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface FileService {

    String readFile(String filePath) throws FileNotFoundException;
    void addToFile(FileName fileName, Object object) throws IOException, ResourceTopilmadi;
    void writeToFile(String filePath, String content) throws IOException;
    List<Object> getObjects(FileName fileName) throws FileNotFoundException, ResourceTopilmadi;


}
