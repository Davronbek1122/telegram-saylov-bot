package uz.pdp;

import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import uz.pdp.bot.BotB21Bot;
import uz.pdp.exeptions.ResourceTopilmadi;
import uz.pdp.model.Party;
import uz.pdp.model.enums.FileName;
import uz.pdp.service.FileService;
import uz.pdp.service.FileServiceImpl;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class Main {
    public static void main(String[] args) throws TelegramApiException {
//        testFileService();
        try {
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
            telegramBotsApi.registerBot(new Saylovchi());
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public static void testFileService() throws ResourceTopilmadi, IOException, ResourceTopilmadi {
        FileService fileService = new FileServiceImpl();
        fileService.addToFile(FileName.PARTY, new Party(UUID.randomUUID(), "Milliy tiklanish", "hammasi yaxshi"));
        List<Object> listParty = fileService.getObjects(FileName.PARTY);
        System.out.println(listParty);

    }
}
