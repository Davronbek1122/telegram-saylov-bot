package uz.pdp;

import lombok.SneakyThrows;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendAnimation;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import uz.pdp.bot.BotService;
import uz.pdp.bot.BotServiceImpl;

public class Saylovchi extends TelegramLongPollingBot {

    public static final BotService botservice = new BotServiceImpl();
    @Override
    public String getBotUsername() {
        return "SaylovvBot";
    }

    @Override
    public String getBotToken() {
        return "5414287808:AAGeijb8DrLlt95ZjUHdYauLkm_2y4tJ2CM";
    }

    @SneakyThrows
    @Override
    public void onUpdateReceived(Update update) {

//        execute(new SendMessage(update.getMessage().getChatId().toString(),"Salom"));
        if(update.getMessage()!=null){
            if(update.getMessage().getContact()!=null){
                execute(botservice.getContact(update));
            }else if(update.getMessage().getLocation()!=null){

            }else{
                String text = update.getMessage().getText();
                if(text.equals("/start")){
                    execute(botservice.getStart(update));
                }else{

                }
            }
        }
        else if (update.getCallbackQuery() != null){
            String data = update.getCallbackQuery().getData();
            if(data.startsWith("Lang:")){
                execute(botservice.getLang(update));
            }

        }
    }
}
