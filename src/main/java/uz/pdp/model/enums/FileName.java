package uz.pdp.model.enums;

public enum FileName {
    CANDIDATE,
    DISTRICT,
    PARTY,
    PARTY_PLAN,
    REGION,
    TG_USER,
    VOTE
}
