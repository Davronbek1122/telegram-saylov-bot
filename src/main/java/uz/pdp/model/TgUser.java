package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.bot.BotState;
import uz.pdp.model.enums.Language;

import java.util.UUID;
@NoArgsConstructor
@AllArgsConstructor
@Data
public class TgUser {
    private UUID id;
    private String fio;
    private String phoneNumber;
    private UUID district_id;
    private Float lon;
    private Float lat;
    private Language language;
    private BotState botState;
    private String chatId;

}
