package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Vote {
    private UUID id;
    private Date date;
    private UUID voter_id;
    private UUID to_candidate_id;
}
