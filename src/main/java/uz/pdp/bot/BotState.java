package uz.pdp.bot;

public enum BotState {
    START,
    CHOOSE_LANG,
    ASK_FIO,
    CONFIRM_FIO,
    CANCEL_FIO,
    SHARE_CONTACT,
    CHOOSE_REGION,
    SHARE_LOCATION,
    SHOW_MENU,
    SHOW_CANDIDATE,
    VOTE,
    SHOW_VOTE_RULE,
    SHOW_RESULT,
    SHOW_PARTY_PLAN
}
