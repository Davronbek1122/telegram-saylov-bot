package uz.pdp.bot;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Contact;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import uz.pdp.exeptions.ResourceTopilmadi;
import uz.pdp.model.TgUser;
import uz.pdp.model.enums.FileName;
import uz.pdp.model.enums.Language;
import uz.pdp.service.FileService;
import uz.pdp.service.FileServiceImpl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BotServiceImpl implements BotService {

    public static final FileService fileService = new FileServiceImpl();

    @Override
    public String getChatIdFromUpdate(Update update) {
        if (update.getMessage() != null) {
            return update.getMessage().getChatId().toString();
        }
        return update.getCallbackQuery().getMessage().getChatId().toString();
    }

    @Override
    public SendMessage generateSendMessageWithInlineKeybordMarkup(String chatId, InlineKeyboardMarkup murkub, String txt) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(txt);
        if (murkub != null) {
            sendMessage.setReplyMarkup(murkub);
        }
        return sendMessage;
    }

    @Override
    public SendMessage generateSendMessageWithReplyKeyboardMarkub(String chatId, ReplyKeyboardMarkup murkub, String txt) {
        SendMessage sendMessage = new SendMessage(chatId, txt);
        sendMessage.setChatId(chatId);
        sendMessage.setText(txt);
        if (murkub != null) {
            sendMessage.setReplyMarkup(murkub);
        }
        return sendMessage;
    }

    @Override
    public SendMessage getStart(Update update) throws IOException {
        String chatId = getChatIdFromUpdate(update);
        TgUser tgUserByChatId = getTgUserByChatId(chatId);
        System.out.println(tgUserByChatId);
        tgUserByChatId.setBotState(BotState.CHOOSE_LANG);
        fileService.addToFile(FileName.TG_USER,tgUserByChatId);

        InlineKeyboardMarkup markup = new InlineKeyboardMarkup();
        List <List<InlineKeyboardButton>> rowList = new ArrayList<>();
        List <InlineKeyboardButton> row1 = new ArrayList<>();
        for (Language value : Language.values()) {
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(value.name());
            button.setCallbackData("Lang:"+ value.name());
            row1.add(button);
        }

        rowList.add(row1);

        markup.setKeyboard(rowList);


        return  generateSendMessageWithInlineKeybordMarkup(
                chatId,
                markup,
                "Choose Lang : \nTil tanlang:");
    }

    @Override
    public TgUser getTgUserByChatId(String chatId) throws IOException {
        List<Object> objects = fileService.getObjects(FileName.TG_USER);
        TgUser tgUser = new TgUser();
        if (objects != null) {
            for (Object object : objects) {
                if (object instanceof TgUser) {
                    TgUser user = (TgUser) object;
                    if (user != null) {
                        if (user.getChatId().equals(chatId)) {
                            return user;
                        }
                    }
                }
            }


        }
        tgUser.setId(UUID.randomUUID());
        tgUser.setChatId(chatId);
        fileService.addToFile(FileName.TG_USER, tgUser);
        return tgUser;
    }

    @Override
    public SendMessage generateSendMessage(String chatId, String text) {
        return new SendMessage(chatId, text);
    }

    @Override
    public SendMessage getContact(Update update) throws IOException {
        String chatId=getChatIdFromUpdate(update);
        TgUser tgUserByChatId = getTgUserByChatId(chatId);
        Contact contact=update.getMessage().getContact();
        tgUserByChatId.setPhoneNumber(contact.getPhoneNumber());
        tgUserByChatId.setFio(contact.getFirstName()+" "+contact.getLastName());
        tgUserByChatId.setBotState(BotState.ASK_FIO);

        fileService.addToFile(FileName.TG_USER,tgUserByChatId);

        String text="";

        if (tgUserByChatId.getLanguage().equals(Language.UZ)){
            text="Ismingiz "+tgUserByChatId.getFio()+" mi? ";
        }else{
            text="Is this your name :" + tgUserByChatId.getFio()+" ? ";
        }

        ReplyKeyboardMarkup markup=new ReplyKeyboardMarkup();
        List<KeyboardRow>keyboardRows=new ArrayList<>();
        KeyboardRow keyboardRow=new KeyboardRow();
        KeyboardButton button=new KeyboardButton();
        KeyboardButton button1=new KeyboardButton();
        if (tgUserByChatId.getLanguage().equals(Language.UZ)){
            button.setText("Ha");
            button.setText("yo'q");
        }else {
            button.setText("Yes");
            button.setText("No");
        }

        keyboardRow.add(button);
        keyboardRow.add(button1);

        keyboardRows.add(keyboardRow);

        markup.setSelective(true);
        markup.setResizeKeyboard(true);
        markup.setKeyboard(keyboardRows);

        return generateSendMessageWithReplyKeyboardMarkub(
                chatId,
                markup,
                text);

    }

    @Override
    public SendMessage getLang(Update update) throws IOException {
        String chatId = getChatIdFromUpdate(update);
        TgUser tgUserByChatId = getTgUserByChatId(chatId);
        String data=update.getCallbackQuery().getData();
        Language language=Language.valueOf(data.substring(5));
        tgUserByChatId.setLanguage(language);
        tgUserByChatId.setBotState(BotState.SHARE_CONTACT);
        fileService.addToFile(FileName.TG_USER, tgUserByChatId);

        String text = ".";
        switch (language){
            case UZ:
                text="Telefon raqamingizni jo'nating.";
                break;
            case ENG:
                text="Share your phone number.";
                break;
        }

        ReplyKeyboardMarkup markup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboardRows = new ArrayList<>();
        KeyboardRow keyboardRow = new KeyboardRow();
        KeyboardButton button = new KeyboardButton();
        if (language.equals(Language.UZ)) {
            button.setText("Tel jo'natish.");
        }
        else {
            button.setText("Share contact.");
        }

        button.setRequestContact(true);

        keyboardRow.add(button);

        keyboardRows.add(keyboardRow);
        markup.setSelective(true);
        markup.setResizeKeyboard(true);
        markup.setKeyboard(keyboardRows);

        return generateSendMessageWithReplyKeyboardMarkub(chatId,
                markup,
                text);
    }

}
