package uz.pdp.bot;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import uz.pdp.model.TgUser;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface BotService {

    String getChatIdFromUpdate(Update update);

    SendMessage generateSendMessageWithInlineKeybordMarkup(String chatId, InlineKeyboardMarkup murkub, String txt);

    SendMessage generateSendMessageWithReplyKeyboardMarkub(String chatId, ReplyKeyboardMarkup murkub, String txt);

    SendMessage getStart(Update update) throws IOException;

    TgUser getTgUserByChatId(String chatId) throws IOException;

    SendMessage generateSendMessage(String chatId, String text);

    SendMessage getContact(Update update) throws IOException;

    SendMessage getLang(Update update) throws IOException;

}
