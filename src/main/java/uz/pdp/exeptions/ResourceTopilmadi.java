package uz.pdp.exeptions;

import java.io.FileNotFoundException;
import java.io.IOException;

public class ResourceTopilmadi extends FileNotFoundException {

    public ResourceTopilmadi(String message) {
        super(message);
    }
}
